# ASUS_VM590LB_X555LB_Hackintosh_EFI_with_OC071

#### 介绍
华硕VM590/X55LB系列OC引导,支持10.9.5~11.4



引导11.4

![输入图片说明](https://images.gitee.com/uploads/images/2021/0909/184129_c6c7e29c_5584424.jpeg "WechatIMG6.jpeg")



引导10.14

![输入图片说明](https://images.gitee.com/uploads/images/2021/0909/184143_b35496d8_5584424.jpeg "WechatIMG3.jpeg")







引导10.9.5(显卡不工作)



![输入图片说明](https://images.gitee.com/uploads/images/2021/0909/184153_9e092e77_5584424.jpeg "WechatIMG7.jpeg")





#### 支持型号
华硕VM590 X555LB




#### 硬件支持

主板: 华硕X555lb

CPU: i7-5500u

iGPU: INTEL Graphics HD5500

DGPU: NVIDIA 940M

声卡: ALC233

无线网卡: 博通 94352HMZ

以太网卡 :Realtek 8168/8111



#### 使用说明

1. 10.14-11.4可直接安装

2. 10.13以下系统,你需要讲机型设置为更老的机型,否则无法安装

3. 10.9.5支持,前提是你有更旧的显卡

   

#### 关于双系统

1.  你可以使用BootCamp启动转换,就像白果那样
2.  你可以常规方法安装windows,但是你必须使用OC引导,才可以在windows安装启动转换助理
3.  双硬盘安装也可以正常切换

